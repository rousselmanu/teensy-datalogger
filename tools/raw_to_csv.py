#!/usr/bin/env python
# encoding: utf-8
# Convert Binary file from Teensy datalogger shield to CSV (TXT) file.

import sys,time,string,bitstring
from bitstring import ConstBitStream

start_time = time.clock()

if __name__ == '__main__':
    infile, outfile = "DATA.BIN", "out.txt"
    fout = open(outfile, 'w')
    fout.write('timestamp\tadcvalue\t\n')

    with open(infile, 'rb') as fin:
        raw_data=ConstBitStream(fin)
        while True:
            try:
                adcvalue=raw_data.readlist('uintle:16')
                timestamp=raw_data.readlist('uintle:16')
                #padding=raw_data.readlist('uintle:16')
            except:
                break
            ##print raw_data.readlist('uint:16')
            fout.write('%d\t%d\t\n' %(timestamp[0], adcvalue[0]))
            fout.flush()

    fin.close();
    fout.close();


