#include <SD.h>
#include <SPI.h>
#include "IntervalTimer.h"
#include "SDstuff.h"
#include "ADC.h"
//#define AUDIO_BOARD

#define ADC_DELAY   500   //time in us between samples
const int A_ONOFF = 22;  //Analog circuit 3.3V regulator ON/OFF
#ifdef AUDIO_BOARD
  const int ANALOG_IN = A6;  //ADC0
#else
  const int ANALOG_IN = A9;  //ADC0
#endif

ADC *adc; // adc object
IntervalTimer timer0;
elapsedMicros t_us;
//RingBuf buffer;
RingBuffer *buf0 = new RingBuffer;

void timer0_callback(void) {  //start ADC measurement
    adc->startSingleRead(ANALOG_IN, ADC_0); 
}

void adc0_isr(void){  //Called when ADC sample is ready
  /*rbData datain;
  datain.timestamp=(uint16_t)(t_us&0xFFFF);  //time in us, 16 bits.
  datain.aData=adc->readSingle();*/
  uint32_t t=t_us;
  uint32_t datain = (t<<16)|(adc->readSingle()&0xFFFF);  //read ADC and add timestamp => 32 bits.
  //rbPutItem(&buffer,datain);
  buf0->write(datain);  //put in buffer
  // restore ADC config if it was in use before being interrupted by the analog timer
  if (adc->adc0->adcWasInUse) {
      // restore ADC config, and restart conversion
      ADC0_CFG1 = adc->adc0->adc_config.savedCFG1;
      ADC0_CFG2 = adc->adc0->adc_config.savedCFG2;
      ADC0_SC2 = adc->adc0->adc_config.savedSC2;
      ADC0_SC3 = adc->adc0->adc_config.savedSC3;
      ADC0_SC1A = adc->adc0->adc_config.savedSC1A;
  }
    //ADC0_RA;  //reset interrupt flag
}

void setup() {
  boolean status;
  
  pinMode(ANALOG_IN, INPUT);   
  pinMode(A_ONOFF, OUTPUT);    
  digitalWrite(A_ONOFF, HIGH); //For use with charge amplifier board: Analog circuit ON
  
  #ifdef AUDIO_BOARD
  SPI.setMOSI(7);
  SPI.setSCK(14);
  #endif
  
  Serial.begin(115200);
  //while(!Serial);
  Serial.println("------------");

  Sd2Card card;
  status = card.init(10); // card SD on pin 10
  if (status) {
    Serial.println("SD card detected :-)");
  } else {
    Serial.println("SD card is not connected or unusable :-(");
    return;
  }

  status = SD.begin(10); // Audio shield has SD card CS on pin 10
  
  startRecording();
  
  adc = new ADC();
  #ifndef AUDIO_BOARD
  adc->setReference(ADC_REF_EXTERNAL, ADC_0); 
  #endif
  adc->setAveraging(32, ADC_0); // set number of averages (1, 4, 8, 16 or 32)
  adc->setResolution(16, ADC_0); // set bits of resolution
  adc->setConversionSpeed(ADC_HIGH_SPEED_16BITS, ADC_0);
  adc->setSamplingSpeed(ADC_HIGH_SPEED_16BITS, ADC_0); // change the sampling speed
  timer0.begin(timer0_callback, ADC_DELAY);//, uSec);
  adc->enableInterrupts(ADC_0);
  //adc->startContinuous(PIEZO_IN, ADC_0);
}


//elapsedMillis ti=0;
void loop(void) {
  continueRecording();
  //delay(1);
  //delayMicroseconds(ADC_DELAY);
  /*if(ti>=100){
    sdfile.flush();
    ti=0;
  }*/
}
